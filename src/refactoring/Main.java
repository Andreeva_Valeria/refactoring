package refactoring;

import java.util.Scanner;

/**
 * Класс для поиска индекса числа в массиве
 *
 * @author Andreeva V.A.
 */
public class Main {
    private static Scanner scanner = new Scanner(System.in);

    private static final int MIN_NUMBER_IN_ARRAY = -25;
    private static final int MAX_RANGE_IN_ARRAY = 51;
    private static final String MESSAGE_ABOUT_LENGTH = "Задайте размер массива: ";
    private static final String MESSAGE_ABOUT_INDEX = "Введите значение элемента, индекс которого хотите найти: ";

    public static void main(String[] args) {

        int length = inputLengthOfArray(MESSAGE_ABOUT_LENGTH);
        int[] array = new int[length];
        randomizeArray(array);

        int element = inputLengthOfArray(MESSAGE_ABOUT_INDEX);

        int result = searchIndexOfElement(array, element);

        if (result == -1) {
            System.out.println("Такого значения нет ");
        } else {
            System.out.println("Индекс искомого элемента в последовательности: " + result);
        }

    }

    /**
     * Возвращает целое число, введенное пользователем из консоли, в ответ на сообщение message
     *
     * @param message сообщение пользователю
     * @return целое число, введенное пользователем из консоли, в ответ на сообщение message
     */
    private static int inputLengthOfArray(String message) {
        System.out.println(message);
        return scanner.nextInt();
    }

    /**
     * Возвращает заполненный рандомными элементами массив
     *
     * @param array пустой массив с заданной длиной
     */
    private static void randomizeArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            array[i] = MIN_NUMBER_IN_ARRAY + (int) (Math.random() * MAX_RANGE_IN_ARRAY);
        }
    }


    /**
     * Возвращает индекс найденного в массиве элемента , иначе -1
     *
     * @param array массив элементов
     * @return индекс найденного в массиве элемента , иначе -1
     */
    private static int searchIndexOfElement(int[] array, int element) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == element) {
                return i;
            }
        }
        return -1;
    }
}

